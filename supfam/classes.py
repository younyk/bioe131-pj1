import urllib2
import re

class Protein(object):
    """A protein object to store all the data about each protein that
    is used when generating the playing cards.

    protein_ID: UniProt accession number (string)
    name: protein name (string)
    species: The organism from which the protein comes (string)
    functions: A list of functions of the protein (list of strings)
    weight: The molecular weight of the protein in Daltons (int)
    length: Length in amino acid residues (int)
    sup_fam: The superfamily to which it belongs (list of Protein objects)
    """

    protein_ID = ''
    name = ''
    species = ''
    functions = None
    weight = 0
    length = 0
    sup_fam = None

    def __init__(self, protein_ID):
        """Creates the Protein object, then calls read_file() to set
        all the instance variables

        protein_ID: The accession number of the protein on UniProt (string)
        """
        super(Protein, self).__init__()
        self.protein_ID = protein_ID
        self.functions = []

        self.read_file(protein_ID)


    def read_file(self, protein_ID):
        """Accessed a file on the UniProt server containing information
        about the protein

        It parses the text file using a set of regular expressions
        looking for the name of the protein, the species of the organism
        from which is comes, up to three functions of the protein if any
        that are in the file, and the length and weight of the protein.
        These are all then stored in the instance variables of this class.

        protein_ID: The accession number of the protein on UniProt (string)
        """

        # set the url of the UniProt file and attempt to open it
        txt_url = 'http://www.uniprot.org/uniprot/%s.txt' %(protein_ID)
        try:
            txt_file = urllib2.urlopen(txt_url)
        except Exception as e:
            print e
        else:  # if no exceptions, i.e. the url loaded
            try:
                p_name = None
                species = None
                functions = []
                weight = None
                length = None

                # read each line in UniProt file
                for line in txt_file:
                    if p_name is None:  # only check this until name is found
                        # matches lines starting with 'DE' and captures the text
                        # after 'Full=', which is the name of the protein
                        name_match = re.search(r'^\s*DE.*Full=([^;/]+)[/;]',line)
                        if name_match:
                            p_name = name_match.group(1)
                        continue  # if this line had name, it can't have the others

                    if species is None:
                        # matches lines starting with 'OS' and captures the text
                        # up to the first '.' or '(', for the species name
                        species_match = re.search(r'^\s*OS\s+([\w\s]+)(?:\w|\s+\()', line)
                        if species_match:
                            species = species_match.group(1)
                        continue

                    if len(functions) < 3:
                        # matches lines starting with 'DR' and captures the text
                        # after 'F:' (for function) up to the first ';'
                        f_match = re.search(r'^\s*DR.+F:([^;]+);', line)
                        if f_match:
                            functions.append(f_match.group(1))

                    if weight is None:
                        # matches lines starting with 'SQ' and captures the text
                        # before 'AA' and 'MW' for length and weight, respectively
                        wl_match = re.search(
                            r'^\s*SQ\s+SEQUENCE\s+(\d+)\s+AA;\s+(\d+)\s+MW;', line)
                        if wl_match:
                            length = int(wl_match.group(1))
                            weight = int(wl_match.group(2))
                        continue
            except Exception as e:
                print e
            finally:  # always close the url when done
                txt_file.close()
        # set the instance variables
        self.name = p_name
        self.species = species
        self.functions = functions
        self.weight = weight
        self.length = length


class SupFam(object):
    """A superfamily object to list proteins in the superfamily
    and store the superfamily name and ID"""

    supfam_ID = ''
    name = ''
    proteins = None

    def __init__(self, supfam_ID, name):
        """Creates the SupFam object with an empty proteins list

        supfam_ID: The superfamily number used by SCOP (string)
        name: The name of the superfamily (string)
        """
        super(SupFam, self).__init__()
        self.name = name
        self.proteins = []
        self.supfam_ID = supfam_ID
        

    def add_protein(self, protein):
        """Adds a protein object to this SupFam object's list

        It also sets the sup_fam instance variable of the protein
        to this SupFam object

        protein: The Protein object to be added (Protein object)
        """
        protein.sup_fam = self
        self.proteins.append(protein)
