#! /usr/bin/python

import os
from classes import Protein, SupFam


def true_rel_path(relative_path=''):
    """Takes the relative_path argument, which is the path
    relative to this file, and converts it to a relative path
    to the directory from which the script is called. Necessary
    for opening files.

    relative_path: The relative path from this file's
                   location to the other file (string)
    """

    return os.path.normpath(
            os.path.join(os.path.dirname(__file__), relative_path))


def parse_supfams():
    """Parses the superfamilies text file to create all SupFam and Protein objects

    Assumes the text file is located at ../superfamilies.txt, and is formatted
    properly. All the SupFam objects will have a list of the Proteins that belong
    to it in their proteins instance variable.
    """

    # get the path of the superfamilies file relative to the terminal location
    supfam_file = true_rel_path('../superfamilies.txt')

    with open(supfam_file, 'r') as file:

        supfam_list = []

        for line in file:
            line = line.strip()

            if '-' in line:  # only superfamily lines have a '-' in them
                split_line = line.split(' - ')

                # make a new SupFam using the ID, name pair from the line and add
                # it to the list of superfamilies to be returned
                current_supfam = SupFam(split_line[0], split_line[1].rstrip(':'))
                supfam_list.append(current_supfam)

            elif line:
                # if it's a nonempty line, but not a superfamily, it must be a protein
                # accession number, so create a Protein and add it to the latest
                # superfamily's list
                current_supfam.add_protein(Protein(line))

    return supfam_list


if __name__ == '__main__':
    """The main function for this script to be run when executed from
    the terminal, creates a LaTeX file from the superfamily list

    Calls a function to parse the list of superfamilies and accession numbers
    and then stores this to a list that is then iterated through to produce
    a LaTeX compatible file with all the protein information
    """

    # The list of colors to use for each superfamily in LaTeX
    colors = ['Bittersweet', 'Apricot', 'DarkOrchid', 'ForestGreen', 'Goldenrod',
              'OrangeRed', 'NavyBlue', 'RoyalBlue', 'Sepia', 'WildStrawberry',
              'Violet', 'SeaGreen', 'YellowOrange']
    # get a list of SupFam objects from the superfamily list text file
    supfams = parse_supfams()

    # create a LaTeX file that has the commands required to make the deck
    with open(true_rel_path('../latex_out/input.tex'), 'w') as file:
        # LaTeX custom command syntax, write LaTeX comment for reference
        file.write('% \protein{{color}protein name}{superfamily ID}'
                   '{superfamily name}{species}{formatted length and '
                   'weight}{protein function 1}{function 2}{function 3}\n')
        for sup_fam in supfams:
            fam_color = colors.pop()  # each family gets a color
            for prot in sup_fam.proteins:
                # make sure prot.functions has 3 strings in it, and isn't completely empty
                if not prot.functions:
                    prot.functions = ['Unlisted function']
                while len(prot.functions) < 3:
                    prot.functions.append('')

                # write the command for this protein
                file.write('\\protein{%s}{%s}{%s}{%s}{%s}{\\underline{Weight:} %d Da, '
                           '\\underline{Length:} %d AA}{%s}{%s}{%s}\n'
                           % (fam_color, prot.name, prot.sup_fam.supfam_ID,
                           prot.sup_fam.name, prot.species, prot.weight, prot.length,
                           prot.functions[0], prot.functions[1], prot.functions[2]))
